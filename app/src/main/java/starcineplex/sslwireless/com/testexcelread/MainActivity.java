package starcineplex.sslwireless.com.testexcelread;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import java.io.InputStream;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity {

    TextView cell0,cell1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cell0 = (TextView) findViewById(R.id.cell0);
        cell1 = (TextView) findViewById(R.id.cell1);

        try {
            InputStream file = getAssets().open("test.xls");
            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                cell0.setText(cell0.getText()+"\n"+row.getCell(0).getNumericCellValue());
                cell1.setText(cell1.getText()+"\n"+row.getCell(1).getStringCellValue());
            }
        } catch (Exception e) {
            Log.e("CheckError", e.getMessage().toString());
        }
    }

}
